import pandas as pd
import numpy as np
from sqlalchemy import create_engine, Table, Column, Integer, String, Float, MetaData, ForeignKey, insert

class PersistMagasin:
    """
    Classe permettant d'enregistrer les données de magasins dans une base de données.
    
    Attributs
    ---------
    mark : str
        marques des magasins.
        
    url : str
        url d'accès au site des magasins.
        
    datas : DataFrame pandas
        DataFrame contenant les données de tous les magasins de la marque.
        La classe attend les données dans les colonne du DataFrame suivant:
        'marque', 'url_marque', 'nom', 'adresse', 'ville', 'code_postal',
        'url_magasin', 'latitude', 'longitude', 'telephone', 'horaire', 'acces',
        'parking', 'produits', 'services'.
        
    engine : sqlalchemy engine
        Objet retourné par la fonction 'create_engine' de sqlalchemy.
        Cet objet représente la connexion à la base de données où les magasins seront enregistrés.
        
    tables : Dict de Table
        Dictionnaire contenant les objets sqlalchemy des tables de la base de données.
            index : nom de la table.
            valeur : table sqlalchemy.            
    """
    
    def __init__(self, mark, url, datas, engine):
        """
        Constructeur de la classe.
        Création de la structure de la base de données.
        
        Parameters
        ----------
        mark : str
            marque des magasins.
        url : str
            url d'accès au site des magasins.
        datas : DataFrame pandas
            DataFrame contenant les données de tous les magasins de la marque.
            La classe attend les données dans les colonne du DataFrame suivant:
            'marque', 'url_marque', 'nom', 'adresse', 'ville', 'code_postal',
            'url_magasin', 'latitude', 'longitude', 'telephone', 'horaire', 'acces',
            'parking', 'produits', 'services'.
        engine : sqlalchemy engine
            Objet retourné par la fonction 'create_engine' de sqlalchemy.
            Cet objet représente la connexion à la base de données où les magasins seront enregistrés.            
            
        """
        self.mark = mark
        self.url = url
        self.datas = datas
        self.engine = engine
        self.tables = {}        
        self.__set_structure()
       
    def __read_table(self, table):
        """
        Retourne dans un DataFrame le contenu de la table dont le nom est passé en argument.
        """
        return pd.read_sql_table(table, self.engine)
    
    def __set_structure(self):
        """
        Défini la structure de la base de données, tables, colonnes et clés primaires.
        """
        metadata_obj = MetaData()
        marques = Table('marques', metadata_obj,
            Column('id_marque', Integer, primary_key=True, autoincrement=True),
            Column('nom', String),
            Column('url', String),
        )
        self.tables['marques']=marques

        produits = Table('produits', metadata_obj,
            Column('id_produit', Integer, primary_key=True, autoincrement=True),
            Column('libelle', String),
        )
        self.tables['produits']=produits
        
        services = Table('services', metadata_obj,
            Column('id_service', Integer, primary_key=True, autoincrement=True),
            Column('libelle', String),
        )
        self.tables['services']=services

        magasins = Table('magasins', metadata_obj,
            Column('id_magasin', Integer, primary_key=True, autoincrement=True),
            Column('id_marque', Integer),
            Column('nom', String),
            Column('adresse', String),
            Column('ville', String),
            Column('code_postal', String),
            Column('url', String),
            Column('latitude', Float),
            Column('longitude', Float),
            Column('telephone', String),
            Column('acces', String),
            Column('parking', String),
        )
        self.tables['magasins']=magasins        
        
        vends = Table('vends', metadata_obj,
            Column('id_magasin', Integer, primary_key=True),
            Column('id_produit', Integer, primary_key=True),
        )
        self.tables['vends']=vends
        
        fournit = Table('fournit', metadata_obj,
            Column('id_magasin', Integer, primary_key=True),
            Column('id_service', Integer, primary_key=True),
        )
        self.tables['fournit']=fournit
        
        horaires = Table('horaires', metadata_obj,
            Column('id_magasin', Integer, primary_key=True),
            Column('jour', String, primary_key=True),
            Column('ouverture', String),
            Column('fermeture', String),
        )
        self.tables['horaires']=horaires
        
        metadata_obj.create_all(self.engine)
        
    
    def __insert(self,table, df, values):
        """
        Insère dans la table dont le nom est passé en argument les données contenues dans le DataFrame.
        Le dictionnaire values fait le lien entre l'identifiant de la colonne du DataFrame et le champ de la table.
        """
        data = []
        for index, row in df.iterrows():
            ligne = "{"
            for key in values.keys():
                ligne += '"' + key  + '":"' + str(row[values[key]]).replace('\n',' ') + '", '
            ligne = ligne[:-2] + "}"
            
            data.append(eval(ligne))
            
        if len(data)>0:
            self.engine.connect().execute(self.tables[table].insert(), data)
    
    def __update(self, table, df, values, where):
        for index, row in df.iterrows():
            v=''
            for key in values.keys():
                v += key + "='" + str(row[values[key]]).replace('\n',' ') + "', "
            v=v[:-2]
            
            w=''
            for key in where.keys():
                w += key + "='" + str(row[where[key]]).replace('\n',' ') + "' AND "
            w=w[:-5]
            
            sql = 'UPDATE ' + table + ' SET ' + v + ' WHERE ' + w + ";"
            self.engine.connect().execute(sql)
            
    def __delete(self, table, df, where):
        for index, row in df.iterrows():
            w=''
            for key in where.keys():
                w += key + "='" + str(row[where[key]]).replace('\n',' ') + "' AND "
            w=w[:-5]
            
            self.engine.connect().execute('DELETE FROM ' + table  + ' WHERE ' + w + ";")

    def set_all_tables(self):
        """
        Enregistrement des données du DataFrame dans la base de données.
        """
        self.set_marque()
        self.set_produit()
        self.set_service()
        self.set_magasin()
        self.set_vends()
        self.set_fournit()
        self.set_horaire()
    
    def get_marque(self, source='base'):
        """
        Retourne un DataFrame contenant les données des marques.
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """
        if source=='base':
            return self.__read_table('marques')
        elif source=='df':
            return pd.DataFrame(self.datas.groupby(['marque','url_marque']).size().reset_index())[['marque','url_marque']].rename(columns={'marque':'nom','url_marque':'url'})
        else :
            return false
    
    def set_marque(self):
        """
        Insère les données dans la table 'marques'
        """
        dfMark = self.get_marque('df').merge(self.get_marque('base'), 
                   how='left', 
                   left_on=['nom','url'], 
                   right_on=['nom','url'])
        
        dfMarkInsert = dfMark[pd.isnull(dfMark['id_marque'])==1]
        self.__insert(
            'marques',
            dfMarkInsert,
            {'nom':'nom', 'url':'url'}
        )
        
        dfMarkUpdate = dfMark[pd.isnull(dfMark['id_marque'])==0].rename(columns={'url_magasin':'url'})
        self.__update(
            'marques',
            dfMarkUpdate,
            {'nom':'nom'},
            {'url':'url'}
        )
    
    def get_produit(self, source='base'):
        """
        Retourne un DataFrame contenant les données des produits.
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """
        if source=='base':
            return self.__read_table('produits')
        elif source=='df':
            return pd.DataFrame(self.datas.produits.explode('produits')).groupby(['produits']).size().reset_index().rename(columns={'produits':'libelle'})
        else :
            return false
        
    def set_produit(self):
        """
        Insère les données dans la table 'produits'
        """        
        dfProduits = self.get_produit('df').merge(self.get_produit('base'), 
                        how='left', 
                        left_on=['libelle'], 
                        right_on=['libelle'])
        dfProduits = dfProduits[pd.isnull(dfProduits['id_produit'])==1]

        self.__insert(
            'produits',
            dfProduits,
            {'libelle':'libelle'}
        )

    def get_service(self, source='base'):
        """
        Retourne un DataFrame contenant les données des services.
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """
        
        if source=='base':
            return self.__read_table('services')
        elif source=='df':
            return pd.DataFrame(self.datas.services.explode('services')).groupby(['services']).size().reset_index().rename(columns={'services':'libelle'})
        else :
            return false
        
    def set_service(self):
        """
        Insère les données dans la table 'services'
        """                
        dfServices = self.get_service('df').merge(self.get_service('base'), 
                        how='left', 
                        left_on=['libelle'], 
                        right_on=['libelle'])
        dfServices = dfServices[pd.isnull(dfServices['id_service'])==1]

        self.__insert(
            'services',
            dfServices,
            {'libelle':'libelle'}
        )

    def get_magasin(self, source='base'):
        """
        Retourne un DataFrame contenant les données des magasins.
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """        
        if source=='base':
            return self.__read_table('magasins')
        elif source=='df':
            return self.datas[['marque','nom','adresse','ville','code_postal','url_magasin','latitude','longitude','telephone','acces','parking']].rename(columns={'url_magasin':'url'})
        else :
            return false
        
    def set_magasin(self):
        """
        Insère les données dans la table 'magasins'
        """                
        dfMagasins = self.get_magasin('df').merge(
            self.get_marque('base')[['id_marque','nom']].rename(columns={'nom':'nom_marque'}), 
            how='inner', 
            left_on=['marque'], 
            right_on=['nom_marque'])
        
        dfMagasins = dfMagasins.merge(
            self.get_magasin('base')[['id_magasin','id_marque','nom','ville']], 
            how='left', 
            left_on=['id_marque','nom','ville'], 
            right_on=['id_marque','nom','ville'])
        
        dfMagasinsInsert = dfMagasins[pd.isnull(dfMagasins['id_magasin'])==1]
        self.__insert(
                'magasins',
                dfMagasinsInsert,
                {'nom':'nom',
                 'adresse':'adresse',
                 'ville':'ville',
                 'code_postal':'code_postal',
                 'url':'url',
                 'latitude':'latitude',
                 'longitude':'longitude',
                 'telephone':'telephone',
                 'id_marque':'id_marque',
                 'parking':'parking',
                 'acces':'acces'
                }
            )
        
        dfMagasinsUpdate = dfMagasins[pd.isnull(dfMagasins['id_magasin'])==0].rename(columns={'url_magasin':'url'})
        self.__update(
            'magasins',
            dfMagasinsUpdate,
            {'adresse':'adresse','url':'url','latitude':'latitude','longitude':'longitude','telephone':'telephone','parking':'parking','acces':'acces'},
            {'id_marque':'id_marque','nom':'nom','ville':'ville'}
        )
                

    def get_vends(self, source='base'):
        """
        Retourne un DataFrame contenant les données de la table vends (relation entre les magasins et les produits).
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """        
        if source=='base':
            return self.__read_table('vends')
        elif source=='df':
            return self.datas[['marque','nom','ville','produits']]
        else :
            return false
        
    def set_vends(self):
        """
        Insère les données dans la table 'vends'
        """                
        dfVends = self.get_vends('df').merge(
            self.get_magasin('base')[['id_magasin','nom','ville']], 
            how='inner', 
            left_on=['nom','ville'], 
            right_on=['nom','ville'])[['id_magasin','produits']]
        
        dfVends = dfVends.explode('produits')
        
        dfVends = dfVends.merge(
            self.get_produit('base'), 
            how='inner', 
            left_on='produits', 
            right_on='libelle')[['id_magasin','id_produit']]
        
        dfVends = dfVends.merge(
            self.get_vends('base').rename(columns={'id_magasin':'table_id_magasin','id_produit':'table_id_produit'}), 
            how='left', 
            left_on=['id_magasin','id_produit'], 
            right_on=['table_id_magasin','table_id_produit'])
        
        dfVends = dfVends.convert_dtypes()
        
        dfVendsUnique = getattr(dfVends.groupby(['id_magasin','id_produit'], as_index=False)[['id_magasin','id_produit']],'sum')()
        self.__delete(
            'vends',
            dfVendsUnique,
            {'id_magasin':'id_magasin','id_produit':'id_produit'}
        )
        
        self.__insert(
            'vends',
            dfVends,
            {'id_magasin':'id_magasin','id_produit':'id_produit'}
        )
        
    def get_fournit(self, source='base'):
        """
        Retourne un DataFrame contenant les données de la table vends (relation entre les magasins et les services).
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """                
        if source=='base':
            return self.__read_table('fournit')
        elif source=='df':
            return self.datas[['marque','nom','ville','services']]
        else :
            return false
            
    def set_fournit(self):
        """
        Insère les données dans la table 'fournit'
        """                
        dfFournit = self.get_fournit('df')
        dfFournit = dfFournit.merge(
            self.get_magasin('base')[['id_magasin','nom','ville']], 
            how='inner', 
            left_on=['nom','ville'], 
            right_on=['nom','ville'])[['id_magasin','services']]
        
        dfFournit = dfFournit.explode('services')
        
        dfFournit = dfFournit.merge(
            self.get_service('base'), 
            how='inner', 
            left_on='services', 
            right_on='libelle')[['id_magasin','id_service']]   
        
        dfFournit = dfFournit.merge(
            self.get_fournit('base').rename(columns={'id_magasin':'table_id_magasin','id_service':'table_id_service'}), 
            how='left', 
            left_on=['id_magasin','id_service'], 
            right_on=['table_id_magasin','table_id_service'])
        #dfFournit = dfFournit[pd.isnull(dfFournit['table_id_magasin'])==1]        
        dfFournit = dfFournit.convert_dtypes()

        dfFournitUnique = getattr(dfFournit.groupby(['id_magasin','id_service'], as_index=False)[['id_magasin','id_service']],'sum')()
        self.__delete(
            'fournit',
            dfFournitUnique,
            {'id_magasin':'id_magasin','id_service':'id_service'}
        )
        
        
        self.__insert(
            'fournit',
            dfFournit,
            {'id_magasin':'id_magasin','id_service':'id_service'}
        )
        
    def get_horaire(self, source='base'):
        """
        Retourne un DataFrame contenant les données de la table horaires (relation entre les magasins et les horaires).
        Si source = 'base', les informations sont récupérées de la base de données.
        Si source = 'df', les données sont récupérées et mises en formes (Lignes et aggrégation) du DataFrame de l'objet.
        """                
        if source=='base':
            return self.__read_table('horaires')
        elif source=='df':
            return self.datas[['nom','ville','horaire']]
        else :
            return false
    
    def __get_horaires_info(self, values, column):
        try:
            return values[column]
        except:
            return ''        
    
    def set_horaire(self):
        """
        Insère les données dans la table 'fournit'
        """                
        dfHoraire = self.get_horaire('df').merge(
            self.get_magasin('base')[['id_magasin','nom','ville']],
            how='inner',
            left_on=['nom','ville'],
            right_on=['nom','ville']
        )[['id_magasin','horaire']]

        dfHoraire = dfHoraire.explode('horaire')
        dfHoraire['jour'] = dfHoraire['horaire'].apply(lambda x: self.__get_horaires_info(x,'jour'))
        dfHoraire['ouverture'] = dfHoraire['horaire'].apply(lambda x: self.__get_horaires_info(x,'ouverture'))
        dfHoraire['fermeture'] = dfHoraire['horaire'].apply(lambda x: self.__get_horaires_info(x,'fermeture'))

        dfHoraire = dfHoraire[dfHoraire['jour']!='']
            
        dfHoraire = dfHoraire.merge(
            self.get_horaire('base').rename(columns={'ouverture':'table_ouverture','fermeture':'table_fermeture'}),
            how='left',
            left_on=['id_magasin','jour'],
            right_on=['id_magasin','jour']
        )
        
        dfHoraireInsert = dfHoraire[pd.isnull(dfHoraire['table_ouverture'])==1]        
        self.__insert(
            'horaires',
            dfHoraireInsert[['id_magasin','jour','ouverture','fermeture']],
            {'id_magasin':'id_magasin','jour':'jour','ouverture':'ouverture','fermeture':'fermeture'}
        )
        
        dfHoraireUpate = dfHoraire[pd.isnull(dfHoraire['table_ouverture'])==0]
        self.__update(
            'horaires',
            dfHoraireUpate[['id_magasin','jour','ouverture','fermeture']],
            {'ouverture':'ouverture','fermeture':'fermeture'},
            {'id_magasin':'id_magasin','jour':'jour'}
        )

# Paramètres de connexion à la base de données.
#   Les variables sont utilisées pour construire la chaine de connexion de l'objet engine
#   de sqlalchemy : engine = create_engine(__driver__://__user__:__password__@__server__:__port__/__database__,connect_args={'options':__options__}) 
base = {
    'driver':'',
    'user':'',
    'password':'',
    'server':'',
    'port':'',
    'database':'',
    'options':'-csearch_path=public',
    'strConnexion':''   # A utiliser lorsque la chaine de connexion ne peux pas être construite : sqlite:///C:\\path\\to\\database\\test.db
}

# Paramètres de démarrage du serveur d'API Flask permettant de visualiser les données contenues
# dans la base.
web = {
    'adresse':'0.0.0.0',
    'port':'5000'
}

# Nom et Url des magasins de l'enseignes à scrapper.
cyrillus = {
    'nom' : 'CYRILLUS',
    'url' : 'https://magasin.cyrillus.fr'
}

# Adresses des serveurs proxies pour les connexions HTTP.
proxies = {"http": "http://55.54.236.154:3128","https": "http://55.54.236.154:3128"}

from flask import Flask, jsonify, abort, make_response, request
from sqlalchemy import create_engine, Table, Column, Integer, String, Float, MetaData, ForeignKey, insert, and_, select
import pandas
import parameters as prm

api = Flask(import_name='magasins')


# Création de la connexion à la base de données.
if prm.base['strConnexion']=='':
    engine = create_engine(
        prm.base['driver'] + '://'+ prm.base['user'] +':'+ prm.base['password'] +'@'+ prm.base['server'] +':'+ prm.base['port'] +'/' + prm.base['database'],
        connect_args={'options': prm.base['options']})        
else:
    engine = create_engine(prm.base['strConnexion'])
        
# Création des tables sqlalchemy pour retrouver les données.
metadata_obj = MetaData()
marques = Table('marques', metadata_obj,
    Column('id_marque', Integer, primary_key=True, autoincrement=True),
    Column('nom', String),
    Column('url', String),
)

produits = Table('produits', metadata_obj,
    Column('id_produit', Integer, primary_key=True, autoincrement=True),
    Column('libelle', String),
)
            
services = Table('services', metadata_obj,
    Column('id_service', Integer, primary_key=True, autoincrement=True),
    Column('libelle', String),
)

magasins = Table('magasins', metadata_obj,
    Column('id_magasin', Integer, primary_key=True, autoincrement=True),
    Column('id_marque', Integer),
    Column('nom', String),
    Column('adresse', String),
    Column('ville', String),
    Column('code_postal', String),
    Column('url', String),
    Column('latitude', Float),
    Column('longitude', Float),
    Column('telephone', String),
    Column('acces', String),
    Column('parking', String),
)
            
vends = Table('vends', metadata_obj,
    Column('id_magasin', Integer, primary_key=True),
    Column('id_produit', Integer, primary_key=True),
)
            
fournit = Table('fournit', metadata_obj,
    Column('id_magasin', Integer, primary_key=True),
    Column('id_service', Integer, primary_key=True),
)
            
horaires = Table('horaires', metadata_obj,
    Column('id_magasin', Integer, primary_key=True),
    Column('jour', String, primary_key=True),
    Column('ouverture', String),
    Column('fermeture', String),
)
metadata_obj.create_all(engine)


@api.errorhandler(404)
def resource_not_found(error):
    return make_response(jsonify({'error': 'Ressource introuvable'}), 404)

@api.errorhandler(400)
def bad_request(error):
    return make_response(jsonify({'error': 'Mauvaise requete'}), 400)

@api.errorhandler(403)
def bad_auth(error):
    return make_response(jsonify({'error': 'Erreur d\'habilitation'}), 403)


@api.route('/status', methods=['GET'])
def return_status():
    return jsonify({'status':'1'})

@api.route('/', methods=['GET'])
@api.route('/enseignes', methods=['GET'])
def get_enseignes():
    stmt = select(marques)
    
    json = ''
    for enseigne in engine.connect().execute(stmt).fetchall():
        json += '<a href="/villes/'+ str(enseigne['id_marque']) +'">'+ enseigne['nom'] +'</a></br>'
        
    return json
    
@api.route('/villes/<id>', methods=['GET'])
def get_villes(id):
    stmt = select(magasins.c.ville).where(magasins.c.id_marque==id).distinct()
    
    json = ''
    for ville in engine.connect().execute(stmt).fetchall():
        json += '<a href="/ville/'+ str(id) +'/'+ ville['ville'] +'">'+ ville['ville'] + '</a></br>'
    
    return json + '</br><a href="/enseignes">Liste des enseignes</>'

@api.route('/ville/<id>/<ville>', methods=['GET'])
def get_magasins(id, ville):
    stmt = select(magasins.c.nom, magasins.c.id_magasin).where(and_(magasins.c.id_marque==id, magasins.c.ville==ville))
    
    json = ''
    for magasin in engine.connect().execute(stmt).fetchall():
        json += '<a href=/magasin/'+ str(magasin['id_magasin']) +'>' + magasin['nom'] + '</a></br>'
    
    return json + '</br><a href="/villes/'+ str(id) +'">Liste des villes</>'

@api.route('/magasin/<id>', methods=['GET'])
def get_magasin(id):
    stmt = select(marques.c.id_marque, marques.c.nom.label('marque'), magasins.c.nom, magasins.c.adresse, magasins.c.code_postal, magasins.c.ville,\
                 magasins.c.latitude, magasins.c.longitude, magasins.c.telephone, magasins.c.acces, magasins.c.parking)\
        .join(marques, magasins.c.id_marque==marques.c.id_marque)\
        .where(magasins.c.id_magasin==id)
    
    json = ''
    magasin = engine.connect().execute(stmt).fetchone()
    
    json += 'Marque : ' + magasin['marque'] + '</br></br>'
    json += 'Nom : ' + magasin['nom'] + '</br></br>'
    json += 'Adresse : ' + magasin['adresse']+ '</br>'
    json += '&nbsp;'* 17 + magasin['code_postal'] + ' ' + magasin['ville'] + '</br></br>'
    json += 'Coordonnées GPS : </br>'
    json += '&nbsp;'* 17 + 'Latitude : ' + str(magasin['latitude']) + '</br>'
    json += '&nbsp;'* 17 + 'Longitude : ' + str(magasin['longitude']) + '</br></br>'
    json += 'Téléphone : ' + magasin['telephone'] + '</br></br>'
    json += 'Acces : ' + magasin['acces'] + '</br></br>'
    json += 'Parking : ' + magasin['parking'] +'</br></br>'
    
    stmt = select(horaires.c.jour, horaires.c.ouverture, horaires.c.fermeture)\
            .join(magasins, magasins.c.id_magasin==horaires.c.id_magasin)\
            .where(magasins.c.id_magasin==id)
    json += 'Horaires : </br>'
    for horaire in engine.connect().execute(stmt).fetchall():
        json += '&nbsp;'* 17 + horaire['jour'] + ' : Ouverture '+ horaire['ouverture'] +' --- Fermeture '+ horaire['fermeture'] +'</br>'
    
    json += '</br></br>Produits proposés : </br>'
    stmt = select(produits.c.libelle)\
            .join(vends, vends.c.id_produit==produits.c.id_produit)\
            .where(vends.c.id_magasin==id)
    for produit in engine.connect().execute(stmt).fetchall():
        json += '&nbsp;'* 17 + produit['libelle'] + '</br>'
        
    json += '</br></br>Services proposés : </br>'
    stmt = select(services.c.libelle)\
            .join(fournit, fournit.c.id_service==services.c.id_service)\
            .where(fournit.c.id_magasin==id)
    for service in engine.connect().execute(stmt).fetchall():
        json += '&nbsp;'* 17 + service['libelle'] + '</br>'
    
    return json + '</br><a href="/ville/'+ str(magasin['id_marque']) +'/'+ magasin['ville'] +'">Liste des magasins à '+ magasin['ville'] +'</>'

if __name__ == '__main__':   
    # Démarrage du server API
    api.run(host=prm.web['adresse'], port=prm.web['port'])
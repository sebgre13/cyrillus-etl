import pandas as pd
import sys
from sqlalchemy import create_engine
from sqlalchemy.exc import SQLAlchemyError

import PersistMagasin as pm
import ScrappingMagasin as scm
import parameters as prm

if __name__ == '__main__':
    print("Extraction")
    # Extraction des infomations des magasins dans le dataframe 'magasins'.
    try:
        cyrillus = scm.cyrillus(
            prm.cyrillus['nom'],
            prm.cyrillus['url'],
            prm.proxies
        )
        magasins = cyrillus.get_magasins()
    except:
        print('Erreur de recupération des données. Vérifier les paramètres.')
        sys.exit()
        
    #### Exemple de creéation d'un magasin manuellement sans scrapping pour tests
    #a = scm.enseignes(
    #    'Leroy Merlin',
    #    'http://www.seb.fr'
    #)
    #a.add_magasin(
    #{
    #            'ville':'ROQUEVAIRE',
    #            'nom':'leroy merlin',
    #            'adresse':'1186 Route de ...',
    #            'code_postal':'13360',
    #            'url_magasin':'www://coucou.fr',
    #            'telephone':'06 00 00 00 00',
    #            'latitude':0,
    #            'longitude':0,
    #            'horaire':[{'jour':'Lundi','ouverture':'08;00', 'fermeture':'19:00'}, {'jour':'Mardi','ouverture':'12:00', 'fermeture':'14:00'}],
    #           'acces':'',
    #            'parking':'',
    #            'produits':['Hommes','Femmes','Enfants'],
    #            'services':['sce1','sce2','sce3']
    #            })
    #magasins = a.get_magasins()
    
    # Créationde la connexion à la base de donnée.
    print("Chargement")
    if prm.base['strConnexion']=='':
        engine = create_engine(
            prm.base['driver'] + '://'+ prm.base['user'] +':'+ prm.base['password'] +'@'+ prm.base['server'] +':'+ prm.base['port'] +'/' + prm.base['database'],
            connect_args={'options': prm.base['options']})        
    else:
        engine = create_engine(prm.base['strConnexion'])        
    
    # Chargement du contenu du dataframe dans la base de donnée.
    try:
        pm.PersistMagasin(
            prm.cyrillus['nom'],
            prm.cyrillus['url'],
            magasins,
            engine
        ).set_all_tables()
    except:
        print('Erreur d\'alimentation des données. Verifier les paramètres.')
        sys.exit()


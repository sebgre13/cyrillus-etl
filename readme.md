# ETL Cyrillus

## Arborescence du projet:
	- parameters.py 		: paramétrages des scripts
	- get_cyrillus.py 		: script d'ETL. Extraction des informations des magasins Cyrillus et 
							Chargement dans la base de données.
	- apiserver.py 			: server d'API Flask permettant de visualiser les données de la base.
	- PersisMagasin.py 		: Classe d'enregistrement des données dans la base.
	- ScrappingMagasin.py 	: Classe de chargement des données des magasins au format attendu par la classe
							d'enregistrement.
	- requirements.txt 		: dépendances python pour le projet.
	- modele.base.png 		: Image du modèle de la base de données.
	
## Préparation de l'environnement:
	- Se positionner dans un environnement Python.
	- Cloner le contenu du projet (6 fichiers) dans un répertoire de l'environnement.
	- Ajouter les dépendances du projet à partir de la ligne de commande suivante :
			pip install -r requirements.txt
	- Paramétrer les scripts à partir des variables contenues dans le fichier parameters.py	
		base : informations de connexion à la base de données.
		web : adresse et port pour accèder au serveur Flask de consultation des données.
		cyrillus : informations sur la marque cyrillus.
		proxies : adresses des serveurs de proxies éventuellement nécessaires pour les connexions HTTP.
		
## Execution des scripts:
	- Démarrage du serveur WEB de consultation à partir de la ligne de commande suivante:
			python path_de_l_environnement/apiserver.py
			
			A la fin de l'exécution l'adresse de consultation des informations est affichée
			a l'écran.
			
	- Execution du script d'ETL sur le site CYRILLUS à partir de la ligne de commande suivante:
			python get_cyrillus.py
			
			La durée d'exécution du script est variable en fonction du volume de données à récupérer,
			de la connexion utilisée et de l'utilisation du serveur WEB consulté.
			L'exécution doit prendre entre 5 et 10 minutes d'après les tests effectués.

import requests
from bs4 import BeautifulSoup
import pandas as pd

class enseignes:
    """
    Classe permettant de récupérer et mettre en forme des informations concernant des enseignes et les magasins associés.
    
    Attributs:
    ----------
    mark : str
        marque de l'enseigne
        
    url : str
        url principale de l'enseigne. Cet url peut être utilisé si les informations retrouvées par scrapping contiennent de chemin relatif.
        
    proxies : dict
        dictionnaire contennant les adresses des serveurs proxies à utiliser pour récupérer les informations par scrapping
    """
    
    def __init__(self, mark, url, proxies={}):
        """
        Constructeur de la classe permettant d'initialiser les variables passées en arguments (marque, url, proxies)
        et le DataFrame pandas qui contiendra les informations des magasins.
        """
        self.mark = mark
        self.url = url
        self.proxies = proxies
        
        self.magasins = pd.DataFrame([],columns={'marque','url_marque','nom','adresse',
                                                 'ville','code_postal','url_magasin','latitude',
                                                 'longitude','telephone','horaire','acces','parking','produits','services'})
        
    def get_elements(self, url, name, attrs):
        """
        Retourne l'objet BeautifulSoup sur la page et répondant aux critères spécifiés dans les arguments.
        
        url : str
            adresse de la page où chercher les informations.
            
        name : str
            nom de la balise HTML à retrouver.
        
        attrs : dict
            attributs HTML pour affiner la recherche des objets à retrouver sur la page
        """
        page = requests.get(url, proxies=self.proxies)
        soup = BeautifulSoup(page.content, 'html.parser')
        return soup.findAll(name=name, attrs=attrs)
    
    def coordonneesGPS(self, street='', city='', postalcode=''):
        """
        Retourne un dictionnaire contenant les informations de géolocalisation d'une adresse.
        
        street : str
            adresse 
            
        city : str 
            ville de l'adresse
            
        postalcode : str
            code postal de la ville
        """        
        
        url = "https://nominatim.openstreetmap.org/search?format=json&addressdetails=1&country=FRANCE"
        if street != '':
            url += "&street="+ street.replace(' ','+')
        if city != '':
            url += "&city="+ city.replace(' ','+')
        if postalcode != '':
            url += "&postalcode="+ postalcode
        response = requests.get(url, proxies=self.proxies)
        if response.status_code == 200:
            return response.json()
        else:
            return False   

    def get_magasins(self):
        """
        Retourne le DataFrame contenant les informations des magasins.
        """
        return self.magasins
    
    def add_magasin(self, datas):
        """
        Fonction permettant d'ajouter un magasin ligne par ligne.
        
        datas : dict
            contient un dictionnaire des données décrivant un magasin:
            {'ville':'abc','nom':'abc','adresse':'abc','code_postal':'12345',
            'url_magasin':'http://xxx.fr','telephone':'00 00 00 00 00',
            'latitude':'','longitude':'','horaire':[''],'acces':[''],'parking':'',
            'produits':[''],'services':['']
            }
        """
        self.magasins = self.magasins.append(datas, ignore_index=True)
        self.magasins['marque'] = self.mark
        self.magasins['url_marque'] = self.url
        
        
    
class cyrillus(enseignes):
    """
    Classe héritée d'ensiegne permettant de récupérer sur le site de Cyrillus les informations de tous les magasins de la marque
    """
    def __init__(self, mark, url, proxies={}):
        super().__init__(mark, url, proxies)
        
        self.villes = pd.DataFrame([],columns={'nom','url'})
        self.__set_villes()
        self.__set_magasins()
    
    def __set_villes(self):
        """
        Fonction permettant de stocker dans un DataFrame sur le site toutes les villes où des magasins sont installés.
        """
        for ville in self.get_elements(
            self.url,'a',{'class': 'lf-footer-seo__container__grid__item__title__link'}):
            self.villes = self.villes.append({'nom':ville.span.text, 'url':ville.get('href')}, ignore_index=True)

    def __get_villes(self):
        """
        Retourne la liste des villes où sont installées des magasins de la marque Cyrillus
        """
        return self.villes
            
    def __set_magasins(self):
        """
        Fonction de scrapping pour récupérer les url de chaque ville puis chaque magasin.
        Pour chaque magasin de la marque trouvé, les infomations sont récupérées et stocké dans le DataFrame de l'instance de la classe
        """
        #for index, row in self.__get_villes()[self.__get_villes()['nom']=='avignon'].iterrows():            
        for index, row in self.__get_villes().iterrows():            
            for magasin in self.get_elements(self.url + row['url'],'li',{'class' : 'lf-location-default lf-geo-divisions__main-content__locations__list__location'}):
                ville = magasin.findAll(name = 'div', attrs={'class':'lf-parts-address__city lf-geo-divisions__main-content__locations__list__location__address__city lf-location-default__content__address__city'})[0].text[6:-1].strip()
                nom = magasin.findAll(name = 'span', attrs = {'class' : 'lf-location-default__content__container__title__text lf-geo-divisions__main-content__locations__list__location__content__container__title__text'})[0].text.strip()
                adresse = magasin.findAll(name = 'div', attrs={'class':'lf-parts-address__street lf-geo-divisions__main-content__locations__list__location__address__street lf-location-default__content__address__street'})[0].text.strip()
                cp = magasin.findAll(name = 'div', attrs={'class':'lf-parts-address__city lf-geo-divisions__main-content__locations__list__location__address__city lf-location-default__content__address__city'})[0].text[0:5].strip()
                url = self.url + magasin.findAll(name = 'a', attrs={'class':'lf-location-default__content--link lf-geo-divisions__main-content__locations__list__location__content--link'})[0].get('href').strip()

                tel = magasin.findAll(name='span', attrs={'class':'lf-location-default__content__bottom__phone__button__label'})
                if len(tel)>0:
                    tel = tel[0].text
                else:
                    tel = ''

                gps = self.coordonneesGPS(street=adresse,city=ville,postalcode=cp)
                if len(gps)>0:
                    lat = gps[0]['lat']
                    lon = gps[0]['lon']
                else:
                    lat=0
                    lon=0

                infos = self.__getInfoMagasin(url)

                self.magasins = self.magasins.append({
                    'marque':self.mark,
                    'url_marque':self.url,
                    'ville':ville,
                    'nom':nom,
                    'adresse':adresse,
                    'code_postal':cp,
                    'url_magasin':url,
                    'telephone':tel,
                    'latitude':lat,
                    'longitude':lon,
                    'horaire':infos['horaires'],
                    'acces':infos['acces'],
                    'parking':infos['parking'],
                    'produits':infos['produits'],
                    'services':infos['services']
                }, ignore_index=True)

    

    def __getHoraire(self, url):
        """
        Retourne un dictionnaire des horaires trouvés dans la page du magasin
        """
        horaires = []
        for jour in self.get_elements(url, 'div', {'class':'lf-location__main-content__hours__opening__content__day'}):
            day = jour.findAll(name='span', attrs={'class':'lf-parts-opening-hours__content__day__name lf-location__main-content__hours__opening__content__day__name'})[0].text

            if len(jour.findAll(name='span', attrs={'class':'lf-parts-opening-hours__content__day__periods__time--inline lf-location__main-content__hours__opening__content__day__periods__time--inline'}))> 0:
                ouverture = jour.findAll(name='span', attrs={'class':'lf-parts-opening-hours__content__day__periods__time--inline lf-location__main-content__hours__opening__content__day__periods__time--inline'})[0].text
                fermeture = jour.findAll(name='span', attrs={'class':'lf-parts-opening-hours__content__day__periods__time--inline lf-location__main-content__hours__opening__content__day__periods__time--inline'})[1].text
            else:
                ouverture ='Fermé'
                fermeture = 'Fermé'

            horaires.append(
                {'jour':day,
                'ouverture':ouverture,
                'fermeture':fermeture})
        return horaires

    def __getAccess(self, url):
        """
        Retourne une chaine de caractère contenant les accès aux magasins trouvés dans la page du magasin.
        """
        acces = self.get_elements(url, 'ul', {'class':'lf-parts-accessibility__list lf-location__main-content__access__map-wrapper__info__access__content__list'})
        if len(acces)>0:
            acces = acces[0].text
        else:
            acces = ''
        return acces

    def __getParking(self, url):
        """
        Retourne une chaine de caractère contenant les parkings trouvés dans la page du magasin.
        """
        parking = self.get_elements(url, 'ul', {'class':'lf-parts-accessibility__list lf-location__main-content__access__map-wrapper__info__parking__content__list'})
        if len(parking)>0:
            parking = parking[0].text
        else:
            parking = ''
        return parking
    
    def __getProduitsetServices(self, url):
        """
        Retourne un dictionnaire contenant les produits et services proposés sur la page du magasin.
        """
        produits=[]
        p = self.get_elements(url, 'ul', {'class':'lf-location__main-content__offers__range__list__wrapper'})
        if len(p)>0:
            for produit in p[0].findAll(name='li'):
                produits.append(produit.findAll(name='h3', attrs={'class':'lf-location__main-content__offers__range__list__item__body__title'})[0].text.replace('\n',''))

        services=[]
        if len(p)>1:
            for service in p[1].findAll(name='li'):
                services.append(service.findAll(name='h3', attrs={'class':'lf-location__main-content__offers__range__list__item__body__title'})[0].text.replace('\n','').strip())
                
        return {'produits':produits,'services':services}
        
    
    def __getInfoMagasin(self, url):
        """
        Retourne par scrapping des informations complémentaires sur un magasin.
        """
        ps = self.__getProduitsetServices(url)

        return {
            'horaires':self.__getHoraire(url), 
            'acces':self.__getAccess(url), 
            'parking':self.__getParking(url), 
            'produits':ps['produits'], 
            'services':ps['services']}    
